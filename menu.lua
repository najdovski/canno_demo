local composer = require("composer")

local scene = composer.newScene()

local function gotoGame()
    composer.removeScene("game")
    composer.gotoScene("game")
end

function scene:create(event)
    local sceneGroup = self.view
    local background = display.newImageRect(sceneGroup, "assets/menu_background.jpg", 568, 320)
    background.x = display.contentCenterX
    background.y = display.contentCenterY

    local title = display.newText(sceneGroup, "CANNO", display.contentCenterX, display.contentCenterY - 50, nil, 80)

    local playButton = display.newImageRect(sceneGroup, "assets/play_button.png", 70, 50)
    playButton.x = display.contentCenterX
    playButton.y = display.contentCenterY + 120

    playButton:addEventListener("tap", gotoGame)
end

function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif (phase == "did") then
    -- Code here runs when the scene is entirely on screen
    -- audio.play( musicTrack, { channel=1, loops=-1 } )
    end
end

function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif (phase == "did") then
    -- Code here runs immediately after the scene goes entirely off screen
    -- audio.stop( 1 )
    end
end

function scene:destroy(event)
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
    -- audio.dispose( musicTrack )
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene
