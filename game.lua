local composer = require("composer")
local Wheel = require("wheel")
local Cannon = require("cannon")

local scene = composer.newScene()

local physics = require("physics")
physics.start()
--physics.setGravity(0.8, 9)

local backGroup, mainGroup
local mergeGroup = display.newGroup()
local cannon, wheel

--The Dot
local dot 
--The Rectangle
local rect

--Math functions
local Cos = math.cos
local Sin = math.sin
local Rad = math.rad
local Atan2 = math.atan2
local Deg = math.deg

local radius = 1
local angle = 90 --Start angle

local function onLocalCollision(self, event)
    local obj1 = event.target
    local obj2 = event.other
    if (event.phase == "began") then
    elseif (event.phase == "ended") then
        if (obj1.myName == "Wheel") then
            print("vo ifot")
            wheel.wheelBall.alpha = 1
            obj2:removeSelf()
        end
    end
end

function fireBall(event)
    if (event.phase == "began") then
        physics.addBody(cannon.ball.ballObj, "dynamic", {density = 0.3, friction = 0.3, bounce = 0.3})
        cannon.ball.ballObj.gravityScale = 0
        cannon.ball.ballObj.isBullet = true
        cannon.ball.ballObj:setLinearVelocity(500, -50)
    elseif event.phase == "ended" then
        cannon.ball = Cannon.createBall()
        cannon.ball.ballObj.myName = "Ball"
    end
end

local doTurn = function(event)
	wheel.wheelObj.x = dot.x  + Cos(Rad(angle)) * radius 
	wheel.wheelObj.y = dot.y  + Sin(Rad(angle)) * radius
	
	local angleBetween = Atan2(dot.y-wheel.wheelObj.y, dot.x-wheel.wheelObj.x)
	wheel.wheelObj.rotation = Deg(angleBetween)
		
	angle = angle - 1	
end

local doTurnBalls = function(event)
	wheel.wheelBall.x = dot.x  + Cos(Rad(angle)) * 90 
	wheel.wheelBall.y = dot.y  + Sin(Rad(angle)) * 90
	
	local angleBetween = Atan2(dot.y-wheel.wheelBall.y, dot.x-wheel.wheelBall.x)
	wheel.wheelBall.rotation = Deg(angleBetween)
		
	angle = angle - 1	
end

dot = display.newCircle(363, 196,0)
dot:setFillColor(200,0,0)

function scene:create(event)
    local sceneGroup = self.view

    backGroup = display.newGroup()
    sceneGroup:insert(backGroup)

    local background = display.newImageRect(backGroup, "assets/game_background.jpg", 568, 320)
    background.x = display.contentCenterX
    background.y = display.contentCenterY

    wheel = Wheel.newWheel(363, 196)
    wheel.wheelObj.myName = "Wheel"
    physics.addBody(wheel.wheelObj, "kinematic", {radius = 60, density = 0.3, friction = 0.3, bounce = 0.3})

    cannon = Cannon.newCannon(43, 253.50, 20, 280)
    cannon.ball.ballObj.myName = "Ball"
    -- physics.addBody(cannon.ball.ballObj, "dynamic", {density = 0.3, friction = 0.3, bounce = 0.3})

    cannon.cannonObj:addEventListener("touch", fireBall)

    wheel.wheelObj.collision = onLocalCollision
    cannon.ball.ballObj.collision = onLocalCollision

    wheel.wheelObj:addEventListener("collision")
    cannon.ball.ballObj:addEventListener("collision")

    -- gameLoopTimer = timer.performWithDelay(100, wheel.rotation, 0)
end

Runtime:addEventListener( "enterFrame", doTurn )
Runtime:addEventListener( "enterFrame", doTurnBalls )
-- local function printFrame()
--     -- print("update")
-- end

-- Runtime:addEventListener( "enterFrame", printFrame )

function scene:show(event)
    local sceneGroup = self.view
    local phase = event.phase
    if (phase == "will") then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
    elseif (phase == "did") then
    -- Code here runs when the scene is entirely on screen
    end
end

-- hide()
function scene:hide(event)
    local sceneGroup = self.view
    local phase = event.phase

    if (phase == "will") then
        -- Code here runs when the scene is on screen (but is about to go off screen)
    elseif (phase == "did") then
    -- Code here runs immediately after the scene goes entirely off screen
    end
end

-- destroy()
function scene:destroy(event)
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
end

scene:addEventListener("create", scene)
scene:addEventListener("show", scene)
scene:addEventListener("hide", scene)
scene:addEventListener("destroy", scene)

return scene
