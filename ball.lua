module(..., package.seeall)
function newBall (ballX, ballY)
    local ball = {}

    ball.ballObj = display.newImageRect("assets/cannon/ball.png",20,20)
    ball.ballObj.anchorX = 0.5;
    ball.ballObj.anchorY = 0.5;
    ball.ballObj.x = ballX
    ball.ballObj.y = ballY

    return ball
end

