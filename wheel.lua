module(..., package.seeall)

function newWheel(wheelX, wheelY)
    local animation = require("plugin.animation")
    local wheelGroup = display.newGroup()
    local wheel = {}

    wheel.wheelObj = display.newImageRect("assets/wheel/wheel.png", 178, 168)
    wheel.wheelObj.anchorX = 0.5;
    wheel.wheelObj.anchorY = 0.5;
    wheel.wheelObj.x = wheelX
    wheel.wheelObj.y = wheelY

    wheel.wheelBall = display.newImageRect("assets/cannon/ball.png",20,20)
    wheel.wheelBall.anchorX = -20
    wheel.wheelBall.anchorY = -20
    wheel.wheelBall.alpha = 0.5
    wheel.wheelBall.x = wheelX-60
    wheel.wheelBall.y = wheelY
    -- function wheel:rotation()
    --     animation.to(wheel.wheelObj, {rotation = wheel.wheelObj.rotation-360}, {time = 5000})
    -- end
    
    return wheel
end



