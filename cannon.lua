module(..., package.seeall)
local Ball = require("ball")

function createBall()
    local ball 
    ball = Ball.newBall(100, 250)
    return ball
end


function newCannon(cannonX, cannonY, wheelX, wheelY)
    local physics = require("physics")
    physics.start()
    local cannon = {}

    local cannonGroup = display.newGroup()
    
    cannon.cannonObj = display.newImageRect(cannonGroup, "assets/cannon/cannon.png", 128, 55)
    cannon.cannonObj.anchorX = 0.5
    cannon.cannonObj.anchorY = 0.5
    cannon.cannonObj.x = cannonX
    cannon.cannonObj.y = cannonY

    cannon.cannonObj.rotation = -10

    cannon.cannonWheel = display.newImageRect(cannonGroup, "assets/cannon/cannon_wheel.png", 80, 80)
    cannon.cannonWheel.anchorX = 0.5
    cannon.cannonWheel.anchorY = 0.5
    cannon.cannonWheel.x = wheelX
    cannon.cannonWheel.y = wheelY
    
    cannon.ball = createBall()
    -- physics.addBody(cannon.ball.ballObj, "kinematic", {density = 0.3, friction = 0.3, bounce = 0.3})
    -- cannon.ball.ballObj.alpha = 0 

    -- cannon.cannonObj:addEventListener("touch", cannon)

    -- function cannon:touch(event)
    --     if (event.phase == "began") then
    --         physics.addBody(cannon.ball.ballObj, "dynamic",{density = 0.3, friction = 0.3, bounce = 0.3})
    --         cannon.ball.ballObj.gravityScale = 0.2
    --         cannon.ball.ballObj.isBullet = true
    --         cannon.ball.ballObj:setLinearVelocity(500, -50)
    --     elseif event.phase == "ended" then
    --         cannon.ball = createBall()
    --         -- cannon.ball.ballObj.alpha = 1
            
            
    --         -- cannon.ball.ballObj.alpha = 0 
    --     end
    -- end

    return cannon
end
